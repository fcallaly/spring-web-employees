package com.citi.training.employees.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.employees.model.Employee;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2")
public class EmployeeControllerIntegrationTests {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeControllerIntegrationTests.class);

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getEmployee_returnsEmployee() {
        restTemplate.postForEntity("/employees", new Employee(-1, "Joe", 123.23), Employee.class);

        ResponseEntity<List<Employee>> getAllResponse = restTemplate.exchange(
                "/employees", HttpMethod.GET, null,
                new ParameterizedTypeReference<List<Employee>>() {});

        logger.info("getAllEmployee response: " + getAllResponse.getBody());

        assertEquals(HttpStatus.OK, getAllResponse.getStatusCode());
        assertTrue(getAllResponse.getBody().get(0).getName().equals("Joe"));
        assertEquals(getAllResponse.getBody().get(0).getSalary(), 123.23, 0.0001);

        // It's not easy to rollback the transactions here as we're interfacing to a web server
        // instead, we'll delete the employee we created.
        restTemplate.delete("/employees/" + getAllResponse.getBody().get(0).getId());
    }

    @Test
    public void getEmployee_returnsNotFound() {

        ResponseEntity<Employee> getByIdResponse = restTemplate.exchange(
                "/employees/9999", HttpMethod.GET, null,
                new ParameterizedTypeReference<Employee>() {});

        logger.info("getByIdEmployee response: " + getByIdResponse.getBody());

        assertEquals(HttpStatus.NOT_FOUND, getByIdResponse.getStatusCode());
    }
}
