package com.citi.training.employees.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.employees.dao.EmployeeDao;
import com.citi.training.employees.model.Employee;

@Component
public class EmployeeService {
    
    @Autowired
    private EmployeeDao employeeDao;

    public List<Employee> findAll() {
        return employeeDao.findAll();
    }

    public Employee findById(int id) {
        return employeeDao.findById(id);
    }

    public Employee create(Employee employee) {
        if(employee.getName().length() > 0) {
            return employeeDao.create(employee);
        }
        throw new RuntimeException("Invalid Parameter: employee name: " +
                                   employee.getName());
    }
 
    public void deleteById(int id) {
        employeeDao.deleteById(id);
    }
}
